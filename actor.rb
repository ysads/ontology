# frozen_string_literal: true

class Actor
  attr_accessor :uid,
                :movies,
                :characters,
                :first_name,
                :family_name,
                :female

  DATA_PROPS = %i[hasFirstName hasFamilyName isFemale].freeze
  OBJ_PROPS = %i[hasInterpreted hasStarred].freeze

  def to_xml
    %(
        <!-- http://www.semanticweb.org/krz/ontologies/cinema##{uid} -->

        <owl:NamedIndividual rdf:about="http://www.semanticweb.org/krz/ontologies/cinema##{uid}">
            <rdf:type rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema#Actor"/>
            #{characters_in_xml}
            #{movies_in_xml}
            <hasFirstName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">#{first_name}</hasFirstName>
            <hasFamilyName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">#{family_name}</hasFamilyName>
            <isFemale rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">#{female}</isFemale>
        </owl:NamedIndividual>
    )
  end

  def characters_in_xml
    return '' if characters.nil?

    characters.map do |character|
      %(<hasInterpreted rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema##{character}"/>)
    end.join("\n            ")
  end

  def movies_in_xml
    return '' if movies.nil?

    movies.map do |movie|
      %(<hasStarred rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema##{movie}"/>)
    end.join("\n            ")
  end
end
