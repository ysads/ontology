# frozen_string_literal: true

class Character
  attr_accessor :uid,
                :role_name,
                :interpreted_by,
                :appeared_in

  DATA_PROPS = %i[hasRoleName].freeze
  OBJ_PROPS = %i[hasAppearedIn isInterpretedBy].freeze

  def to_xml
    %(
        <!-- http://www.semanticweb.org/krz/ontologies/cinema##{uid} -->

        <owl:NamedIndividual rdf:about="http://www.semanticweb.org/krz/ontologies/cinema##{uid}">
            <rdf:type rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema#Character"/>
            <hasAppearedIn rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema##{appeared_in}"/>
            <isInterpretedBy rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema##{interpreted_by}"/>
            <hasRoleName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">#{role_name}</hasRoleName>
        </owl:NamedIndividual>
    )
  end
end
