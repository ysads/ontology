# MAC0444 - Ontologia

## Requirements

It's required to have ruby installed on your machine. You can check that by running `ruby -v` and expecting to see the current ruby version.

If you don't have it already, you can install it using your OS package manage. For example:

```sh
brew install ruby      # Mac OS
pacman install ruby    # Arch
apt-get install ruby   # Debian
```

## Parsing

To parse individuals from `filmes.pl` database, use the following command:

```sh
ruby main.rb
```

That should generate a file named `filmes.xml` containing all the parsed entities correctly represented as in OWL ontology.