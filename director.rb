# frozen_string_literal: true

class Director
  attr_accessor :uid,
                :movies,
                :first_name,
                :family_name

  DATA_PROPS = %i[hasFirstName hasFamilyName].freeze
  OBJ_PROPS = %i[hasDirected].freeze

  def to_xml
    %(
        <!-- http://www.semanticweb.org/krz/ontologies/cinema##{uid} -->

        <owl:NamedIndividual rdf:about="http://www.semanticweb.org/krz/ontologies/cinema##{uid}">
            <rdf:type rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema#Director"/>
            #{movies_in_xml}
            <hasFirstName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">#{first_name}</hasFirstName>
            <hasFamilyName rdf:datatype="http://www.w3.org/2001/XMLSchema#string">#{family_name}</hasFamilyName>
        </owl:NamedIndividual>
    )
  end

  def movies_in_xml
    return '' if movies.nil?

    movies.map do |movie|
      %(<hasDirected rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema##{movie}"/>)
    end.join("\n            ")
  end
end
