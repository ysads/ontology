# frozen_string_literal: true

require_relative 'actor'
require_relative 'character'
require_relative 'director'
require_relative 'movie'

class ProtegeParser
  attr_reader :entities

  ENTITIES = %w[actor character director movie].freeze

  def initialize(file)
    @file = file
    @entities = {
      'actor' => {},
      'character' => {},
      'director' => {},
      'movie' => {}
    }
  end

  def parse
    @file.read.each_line do |line|
      next if comment?(line)

      entity_name = extract_entity_name(line)
      tokens = parse_line(line, entity: entity_name)

      case entity_name
      when 'filme'
        new_movie(tokens)
      when 'diretor'
        new_director(tokens)
      when 'ator', 'atriz'
        new_actor(tokens, female: (entity_name == 'atriz'))
      when 'tit_dur'
        update_movie_dur(tokens)
      when 'nome'
        update_names(tokens)
      end
    end
  end

  def write_xml(file_out)
    ENTITIES.each do |entity_class|
      @entities[entity_class].each do |_, entity|
        file_out.write(entity.to_xml) if entity.respond_to?(:to_xml)
      end
    end
  end

  private

  def comment?(line)
    line == "\n" ||
      line.include?('%') ||
      line.include?(':-')
  end

  def find(klass, entity_uid)
    @entities[klass.to_s.downcase][entity_uid]
  end

  def find_or_new(klass, entity_uid)
    find(klass, entity_uid) || klass.new
  end

  def persist(entity)
    klass = entity.class
    @entities[klass.to_s.downcase][entity.uid] = entity
  end

  def extract_entity_name(line)
    line.gsub(/\s|\)|\./, '').split(/\(/).first
  end

  def parse_line(line, entity:)
    sanitizer = if %w[tit_dur nome].include?(entity)
                  /\(|\)|\.|\n|\'/
                else
                  /\s|\(|\)|\.|\n|\'/
                end

    line.gsub(entity, '')
        .gsub(sanitizer, '')
        .split(',')
  end

  def new_movie(tokens)
    movie = Movie.new.tap do |m|
      m.uid = tokens.first
      m.title = tokens.first
      m.release_year = tokens.last.to_i
    end

    persist(movie)
  end

  def new_director(tokens)
    movie = find(Movie, tokens.first)
    director_name = tokens.last

    director = find_or_new(Director, director_name)
    director.uid = director_name
    director.movies = Array(director.movies).unshift(movie.title)

    movie.directors = Array(movie.directors).unshift(director.uid)

    persist(director)
    persist(movie)
  end

  def new_actor(tokens, female:)
    movie_uid = tokens[0]
    actor_uid = tokens[1]
    role_name = tokens[2]

    movie = find(Movie, movie_uid)
    character = Character.new.tap do |c|
      c.uid = "#{actor_uid}_#{role_name}"
      c.role_name = role_name
      c.interpreted_by = actor_uid
      c.appeared_in = movie_uid
    end

    actor = find_or_new(Actor, actor_uid)
    actor.uid = actor_uid
    actor.female = female
    actor.movies = Array(actor.movies).unshift(movie.uid).uniq
    actor.characters = Array(actor.characters).unshift(character.uid).uniq

    movie.actors = Array(movie.actors).unshift(actor.uid)
    movie.characters = Array(movie.characters).unshift(character.uid)

    persist(character)
    persist(actor)
    persist(movie)
  end

  def update_movie_dur(tokens)
    movie = find(Movie, tokens.first)

    movie.title = tokens[1].strip
    movie.running_time = tokens[2].strip.to_i

    persist(movie)
  end

  def update_names(tokens)
    update_entity_names(Actor, tokens)
    update_entity_names(Director, tokens)
  end

  def update_entity_names(klass, tokens)
    entity = find(klass, tokens.first)
    return unless entity

    entity.first_name = tokens[1].strip
    entity.family_name = tokens[2].strip

    persist(entity)
  end
end
