# frozen_string_literal: true

require_relative 'protege_parser'
require 'awesome_print'
require 'byebug'

file_in = File.open('filmes.pl', 'r')
file_out = File.open('filmes.xml', 'w')

parser = ProtegeParser.new(file_in)
parser.parse
parser.write_xml(file_out)

file_out.close
file_in.close
