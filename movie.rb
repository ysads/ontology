# frozen_string_literal: true

class Movie
  attr_accessor :uid,
                :characters,
                :directors,
                :actors,
                :title,
                :running_time,
                :release_year

  DATA_PROPS = %i[hasReleaseYear hasRunningTime hasTitle].freeze
  OBJ_PROPS = %i[isDirectedBy isStarredBy].freeze

  def to_xml
    %(
        <!-- http://www.semanticweb.org/krz/ontologies/cinema##{uid} -->

        <owl:NamedIndividual rdf:about="http://www.semanticweb.org/krz/ontologies/cinema##{uid}">
            <rdf:type rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema#Movie"/>
            #{directors_in_xml}
            #{characters_in_xml}
            #{actors_in_xml}
            <hasTitle rdf:datatype="http://www.w3.org/2001/XMLSchema#string">#{xml_safe_title}</hasTitle>
            <hasReleaseYear rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">#{release_year}</hasReleaseYear>
            <hasRunningTime rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">#{running_time}</hasRunningTime>
        </owl:NamedIndividual>
    )
  end

  def xml_safe_title
    title.gsub(/\&/, "and")
  end

  def characters_in_xml
    return '' if characters.nil?

    characters.map do |character|
      %(<hasFeatured rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema##{character}"/>)
    end.join("\n            ")
  end

  def directors_in_xml
    return '' if directors.nil?

    directors.map do |director|
      %(<isDirectedBy rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema##{director}"/>)
    end.join("\n            ")
  end

  def actors_in_xml
    return '' if actors.nil?

    actors.map do |actor|
      %(<isStarredBy rdf:resource="http://www.semanticweb.org/krz/ontologies/cinema##{actor}"/>)
    end.join("\n            ")
  end
end
